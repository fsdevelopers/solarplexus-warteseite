<!DOCTYPE html>
<html>
<head>
	<title>Solarplexus-Warteseite</title>
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
</head>
	<body>

			<div class="content">
				<header><img src="img/logo.svg" alt="" class="logo"></header>
				<div class="main datenschutz">
					<h1>Datenschutzerklärung</h1>
					<p>Diese Datenschutzerklärung klärt Sie über die Art, den Umfang und Zweck der Verarbeitung von personenbezogenen Daten innerhalb unseres Onlineangebotes auf. Verantwortlicher im Sinne des Art. 4 der Datenschutzgrundverordnung (DSGVO) ist:</p>

					<strong>SOLARplexus Dach GmbH</strong>

					<p>
						Am Witschenberg 2<br />
						 57271 Hichenbach<br />
						T:<a href="phone:+4915123564080">+49 151 23564080</a><br />
						<a href="mailto:info@solarplexus.group"> info@solarplexus.group</a></p>
					<h2>I. Allgemeines zur Datenverarbeitung</h2>
					<h3>1. Umfang der Verarbeitung Personenbezogener Daten</h3>
					<p>Wir verarbeiten personenbezogene Daten unserer Nutzer grundsätzlich nur, soweit dies zur Bereitstellung einer funktionsfähigen Webseite sowie unserer Inhalte und Leistungen erforderlich ist. Die Verarbeitung personenbezogener Daten unserer Nutzer erfolgt regelmäßig nur nach Einwilligung des Nutzers. Eine Ausnahme gilt in solchen Fällen, in denen eine vorherige Einholung einer Einwilligung aus tatsächlichen Gründen nicht möglich ist und die Verarbeitung der Daten durch gesetzliche Vorschriften gestattet ist.</p>

					<p>Art der verarbeiteten Daten:
					<ul>
						<li>Bestandsdaten (z.B. Namen)</li>
						<li>Kontaktdaten (z.B. E-Mail-Adressen)</li>
						<li>Inhaltsdaten (z.B. Texteingaben, Fotografien)</li>
						<li>Nutzungsdaten (z.B. Zugriffszeiten auf die Webseite)</li>
						<li>Meta-/Kommunikationsdaten (z.B. IP-Adressen)</li>
					</ul>

					<h3>2. Rechtsgrundlage für die Verarbeitung personenbezogener Daten</h3>
					<p>Soweit wir für die Verarbeitungsvorgänge personenbezogener Daten eine Einwilligung der betroffenen Person einholen, dient Art. 6 Abs. 1 lit a EU-Datenschutzgrundverordnung („DSGVO“) als Rechtsgrundlage. Bei der Verarbeitung von personenbezogenen Daten, die zur Erfüllung eines Vertrages, dessen Vertragspartei die betroffene Person ist, erforderlich ist, dient Art. 6 Abs. 1 lit. b DSGVO als Rechtsgrundlage. Dies gilt auch für Verarbeitungsvorgänge, die zur Durchführung vorvertraglicher Maßnahmen erforderlich sind. Soweit eine Verarbeitung personenbezogener Daten zur Erfüllung einer rechtlichen Verpflichtung erforderlich ist, der unser Unternehmen unterliegt, dient Art. 6 Abs. 1 lit. c DSGVO als Rechtsgrundlage. Für den Fall, dass lebenswichtige Interessen der betroffenen Person oder einer anderen natürlichen Person eine Verarbeitung personenbezogener Daten erforderlich machen, dient Art. 6 Abs. 1 lit. d DSGVO als Rechtsgrundlage. Ist die Verarbeitung zur Wahrung eines berechtigten Interesses unseres Unternehmens oder eines Dritten erforderlich und überwiegen die Interessen, Grundrechte und Grundfreiheiten des Betroffenen das erstgenannte Interesse nicht, so dient Art. 6 Abs. 1 lit. f DSGVO als Rechtsgrundlage für die Verarbeitung.</p>

					<h3>3. Datenlöschung und Speicherdauer</h3>
					<p>Die personenbezogenen Daten der betroffenen Person werden gelöscht oder gesperrt, sobald der Zweck der Speicherung entfällt. Eine Speicherung kann darüber hinaus erfolgen, wenn dies durch den europäischen oder nationalen Gesetzgeber in unionsrechtlichen Verordnungen, Gesetzen oder sonstigen Vorschriften, denen der Verantwortliche unterliegt, vorgesehen wurde. Eine Sperrung oder Löschung der Daten erfolgt auch dann, wenn eine durch die genannten Normen vorgeschriebene Speicherfrist abläuft, es sei denn, dass eine Erforderlichkeit zur weiteren Speicherung der Daten für einen Vertragsabschluss oder eine Vertragserfüllung besteht.</p>
					<h2>II. Bereitstellung der Webseite und Erstellung von Logfiles</h2>
					<p>Bei jedem Aufruf unserer Internetseite erfasst unser System automatisiert Daten und Informationen vom Computersystem des aufrufenden Rechners. Der Provider der Seite erhebt und speichert Informationen in so genannten Server-Log-Dateien, die Ihr Browser automatisch an uns übermittelt. Dies sind:</p>
					<ul>
						<li>Browsertyp und Browserversion</li>
						<li>verwendetes Betriebssystem</li>
						<li>Referrer URL</li>
						<li>Hostname des zugreifenden Rechners</li>
						<li>Datum und Uhrzeit der Serveranfrage</li>
					</ul>

					<h3>1. Rechtsgrundlage für die Datenverarbeitung</h3>
					<p>Grundlage für die Datenverarbeitung ist Art. 6 Abs. 1 lit. f DSGVO.</p>
					<p>Zweck der Datenverarbeitung</p>
					<p>Die vorübergehende Speicherung der IP-Adresse durch das System ist notwendig, um eine Auslieferung der Webseite an den Rechner des Nutzers zu ermöglichen. Hierfür muss die IP-Adresse des Nutzers für die Dauer der Sitzung gespeichert bleiben. Die Speicherung in Log-Files erfolgt, um die Funktionsfähigkeit der Webseite sicherzustellen. Zudem dienen uns die Daten zur Optimierung der Webseite und zur Sicherstellung bzw. zur Sicherheit unserer informationstechnischen Systeme. In diesen Zwecken liegt auch unser berechtigtes Interesse an der Datenverarbeitung nach Art. 6 Abs. 1 lit. f. DSGVO.</p>

					<h3>2. Dauer der Speicherung</h3>
					<p>Die Daten werden gelöscht, sobald sie für die Erreichung des Zwecks ihrer Erhebung nicht mehr erforderlich sind. Im Falle der Erfassung der Daten zur Bereitstellung der Webseite ist dies der Fall, wenn die jeweilige Sitzung beendet ist.</p>

					<h3>3. Widerspruchs- und Beseitigungsmöglichkeit</h3>
					<p>Die Erfassung der Daten zur Bereitstellung der Webseite und die Speicherung der Daten in Log-Files ist für den Betrieb der Internetseite zwingend erforderlich. Es besteht folglich seitens des Nutzers keine Widerspruchsmöglichkeit.</p>

					<h2>III. Cookies</h2>
					<p>Unsere Webseite verwendet Cookies. Bei Cookies handelt es sich um Textdateien, die im Internetbrowser bzw. vom Internetbrowser auf dem Computersystem des Nutzers gespeichert werden. Ruft ein Nutzer eine Webseite auf, so kann ein Cookie auf dem Betriebssystem des Nutzers gespeichert werden. Die meisten der von uns verwendeten Cookies sind so genannte “Session-Cookies”. Sie werden nach Ende Ihres Besuchs automatisch gelöscht. Andere Cookies bleiben auf Ihrem Endgerät gespeichert bis Sie diese löschen.</p>
					<p>Sie können Ihren Browser so einstellen, dass Sie über das Setzen von Cookies informiert werden und Cookies nur im Einzelfall erlauben, die Annahme von Cookies für bestimmte Fälle oder generell ausschließen sowie das automatische Löschen der Cookies beim Schließen des Browser aktivieren. Bei der Deaktivierung von Cookies kann die Funktionalität unserer Website eingeschränkt sein.</p>
					<p>Jeder Browser nutzt unterschiedliche Verfahren für die Verwaltung der Einstellungen, Cookies und Website-Daten. Weitere Informationen sind unter folgende Links ersichtlich:</p>
					<ul>
						<li>Chrome:  <a href="">https://support.google.com/chrome/answer/95647?hl=EN</a></li>
						<li>Firefox:  <a href="">https://support.mozilla.org/en-US/kb/cookies-information-websites-store-on-your-computer</a></li>
						<li>Internet Explorer:  <a href="">http://windows.microsoft.com/en-us/windows7/how-to-manage-cookies-in-internet-explorer-9</a></li>
						<li>Opera:  <a href="">http://help.opera.com/Windows/10.00/en/cookies.html</a></li>
						<li>Safari:  <a href="">https://support.apple.com/en-us/HT201265</a></li>
					</ul>

					<p>Cookies, die zur Durchführung des elektronischen Kommunikationsvorgangs oder zur Bereitstellung bestimmter, von Ihnen erwünschter Funktionen (z.B. Warenkorbfunktion) erforderlich sind, werden auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO gespeichert. Der Webseitenbetreiber hat ein berechtigtes Interesse an der Speicherung von Cookies zur technisch fehlerfreien und optimierten Bereitstellung seiner Dienste. Soweit andere Cookies (z.B. Cookies zur Analyse Ihres Surfverhaltens) gespeichert werden, werden diese in dieser Datenschutzerklärung gesondert behandelt.</p>

					<h2>IV. Kontaktformular</h2>
					<h3>1. Beschreibung und Umfang der Datenverarbeitung</h3>
					<p>Auf unserer Internetseite ist ein Kontaktformular vorhanden, welches freiwillig für die elektronische Kontaktaufnahme genutzt werden kann. Die im Kontaktformular abgefragten Daten dienen ausschließlich dem Zweck der Bearbeitung des Anliegens/der Anfrage des Nutzers.</p>
					<p>Nimmt ein Nutzer die Möglichkeit des Kontaktformulars wahr, so werden die in der Eingabemaske eingegebenen Daten an uns übermittelt und gespeichert. Diese Daten sind:</p>
					<ul>
						<li>Name</li>
						<li>E-Mail-Adresse (Pflichtfeld)</li>
						<li>Betreff</li>
						<li>Nachricht (Pflichtfeld)</li>
					</ul>
					<p>Im Zeitpunkt der Absendung der Nachricht werden zudem folgende Daten gespeichert:</p>
					<ul>
						<li>Datum und Uhrzeit der Registrierung</li>
						<li>Absender-E-Mail-Adresse</li>
					</ul>
					<p>Alternativ ist eine Kontaktaufnahme über die bereitgestellte E-Mail-Adresse möglich. In diesem Fall werden die mit der E-Mail übermittelten personenbezogenen Daten des Nutzers gespeichert.</p>
					<p>Es erfolgt in diesem Zusammenhang keine Weitergabe der Daten an Dritte.</p>

					<h3>2. Rechtsgrundlage für die Datenverarbeitung</h3>
					<p>Rechtsgrundlage für die Verarbeitung der Daten ist bei Vorliegen einer Einwilligung des Nutzers Art. 6 Abs. 1 lit. a DSGVO. Rechtsgrundlage für die Verarbeitung der Daten, die im Zuge einer Übersendung einer E-Mail übermittelt werden, ist Art. 6 Abs. 1 lit. f DSGVO. Zielt ein E-Mail-Kontakt auf den Abschluss eines Vertrages ab, so ist zusätzliche Rechtsgrundlage für die Verarbeitung Art. 6 Abs. 1 lit. b DSGVO.</p>
					<p>Die Verarbeitung der in das Kontaktformular eingegebenen Daten dient uns allein zur Bearbeitung der Kontaktaufnahme. Im Falle einer Kontaktaufnahme per E-Mail liegt hieran auch das erforderliche berechtigte Interesse an der Verarbeitung der Daten. Die sonstigen während des Absendevorgangs verarbeiteten personenbezogenen Daten dienen dazu, einen Missbrauch des Kontaktformulars und die Sicherheit unserer informationstechnischen Systeme sicherzustellen.</p>
					<p>Die Daten werden nach Ablauf der handels- und steuerrechtlichen Aufbewahrungspflichten gelöscht.</p>

					<h3>3. Widerspruchs- und Beseitigungsmöglichkeit</h3>
					<p>Der Nutzer hat jederzeit die Möglichkeit, seine Einwilligung zur Verarbeitung der personenbezogenen Daten zu widerrufen. Nimmt der Nutzer per E-Mail Kontakt mit uns auf, so kann er der Speicherung seiner personenbezogenen Daten jederzeit widersprechen. In einem solchen Fall kann die Konversation nicht fortgeführt werden. Alle personenbezogenen Daten, die im Zuge der Kontaktaufnahme gespeichert wurden, werden in diesem Fall gelöscht.</p>

					<h2>V. Soziale Medien</h2>
					<p>Wir unterhalten Onlinepräsenzen innerhalb sozialer Plattformen, um mit den dort aktiven Kunden, Interessenten und Nutzern zu kommunizieren und sie dort über unsere Leistungen informieren zu können. Beim Aufruf der jeweiligen Netzwerke und Plattformen gelten die Geschäftsbedingungen und die Datenverarbeitungsrichtlinien deren jeweiligen Betreiber.</p>

					<h3>1. Datenschutzerklärung für die Nutzung von Facebook-Plugins  (Like-Button)</h3>
					<p>Wir nutzen auf Grundlage unserer berechtigten Interessen (d.h. Interesse an der Analyse, Optimierung und wirtschaftlichem Betrieb unseres Onlineangebotes im Sinne des Art. 6 Abs. 1 lit. f DSGVO) auf unseren Seiten Plugins des sozialen Netzwerks Facebook, 1601 South California Avenue, Palo Alto, CA 94304, USA. Die Facebook-Plugins erkennen Sie an dem Facebook-Logo oder dem “Like-Button” (“Gefällt mir”) auf unserer Seite. Eine Übersicht über die Facebook-Plugins finden Sie hier: <a href="http://developers.facebook.com/docs/plugins/">http://developers.facebook.com/docs/plugins/.</a></p>
					<p>Facebook ist unter dem Privacy-Shield-Abkommen zertifiziert und bietet hierdurch eine Garantie, das europäische Datenschutzrecht einzuhalten (<a href="https://www.privacyshield.gov/participant?id=a2zt0000000GnywAAC&status=Active">https://www.privacyshield.gov/participant?id=a2zt0000000GnywAAC&status=Active</a>).</p>
					<p>Wenn ein Nutzer eine Funktion dieses Onlineangebotes aufruft, die ein solches Plugin enthält, baut sein Gerät eine direkte Verbindung mit den Servern von Facebook auf. Der Inhalt des Plugins wird von Facebook direkt an das Gerät des Nutzers übermittelt und von diesem in das Onlineangebot eingebunden. Dabei können aus den verarbeiteten Daten Nutzungsprofile der Nutzer erstellt werden. Wir haben daher keinen Einfluss auf den Umfang der Daten, die Facebook mit Hilfe dieses Plugins erhebt und informiert die die Nutzer daher entsprechend unserem Kenntnisstand.</p>
					<p>Durch die Einbindung der Plugins erhält Facebook die Information, dass ein Nutzer die entsprechende Seite des Onlineangebotes aufgerufen hat. Ist der Nutzer bei Facebook eingeloggt, kann Facebook den Besuch seinem Facebook-Konto zuordnen. Wenn Nutzer mit den Plugins interagieren, z.B. den Like Button betätigen oder einen Kommentar abgeben, wird die entsprechende Information von Ihrem Gerät direkt an Facebook übermittelt und dort gespeichert. Falls ein Nutzer kein Mitglied von Facebook ist, besteht trotzdem die Möglichkeit, dass Facebook seine IP-Adresse in Erfahrung bringt und speichert. Laut Facebook wird in Deutschland nur eine anonymisierte IP-Adresse gespeichert.</p>
					<p>Zweck und Umfang der Datenerhebung und die weitere Verarbeitung und Nutzung der Daten durch Facebook sowie die diesbezüglichen Rechte und Einstellungsmöglichkeiten zum Schutz der Privatsphäre der Nutzer, können diese den Datenschutzhinweisen von Facebook entnehmen: <a href="http://de-de.facebook.com/policy.php">http://de-de.facebook.com/policy.php</a></p>
					<p>Wenn ein Nutzer Facebook-Mitglied ist und nicht möchte, dass Facebook über dieses Onlineangebot Daten über ihn sammelt und mit seinen bei Facebook gespeicherten Mitgliedsdaten verknüpft, muss er sich vor der Nutzung unseres Onlineangebotes bei Facebook ausloggen und seine Cookies löschen. Weitere Einstellungen und Widersprüche zur Nutzung von Daten für Werbezwecke, sind innerhalb der Facebook-Profileinstellungen möglich: <a href="https://www.faceboo.com/settings?tab=ads">https://www.faceboo.com/settings?tab=ads</a> oder über die US-amerikanische Seite <a href="http://www.aboutads.info/choices/">http://www.aboutads.info/choices/</a> oder die EU-Seite <a href="http://www.youronlinechoices.com/">http://www.youronlinechoices.com/</a>. Die Einstellungen erfolgen plattformunabhängig, d.h. sie werden für alle Geräte, wie Desktopcomputer oder mobile Geräte übernommen.</p>

					<h3>2. Datenschutzerklärung für die Nutzung von Instagram</h3>
					<p>Auf dieser Webseite sind Funktionen des Dienstes Instagram eingebunden. Diese Funktionen werden angeboten durch die Instagram Inc., 1601 Willow Road, Menlo Park, CA, 94025, USA integriert. Wenn Sie in Ihrem Instagram – Account eingeloggt sind können Sie durch Anklicken des Instagram – Buttons die Inhalte unserer Seiten mit Ihrem Instagram – Profil verlinken. Dadurch kann Instagram den Besuch unserer Seiten Ihrem Benutzerkonto zuordnen. Wir weisen darauf hin, dass wir als Anbieter der Seiten keine Kenntnis vom Inhalt der übermittelten Daten sowie deren Nutzung durch Instagram erhalten.</p>
					<p>Weitere Informationen hierzu finden Sie in der Datenschutzerklärung von Instagram: <a href="http://instagram.com/about/legal/privacy/">http://instagram.com/about/legal/privacy/</a></p>

					<h2>VI. Datenschutzerklärung für die Nutzung von Google Analytics</h2>
					<p>Unsere Website benutzt Google Analytics, einen Webanalysedienst der Google Inc. (“Google”). Google Analytics verwendet sog. “Cookies”, Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Sie ermöglichen. Die durch den Cookie erzeugten Informationen über Ihre Benutzung dieser Website werden in der Regel an einen Server von Google in den USA übertragen und dort gespeichert. Die Speicherung von Google-Analytics-Cookies erfolgt auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO. Der Webseitenbetreiber hat ein berechtigtes Interesse an der Analyse des Nutzerverhaltens, um sowohl sein Werbeangebot als auch seine Werbung zu optimieren.</p>
					<h3>IP-Anonymisierung</h3>
					<p>Wir haben auf dieser Website die Funktion IP-Anonymisierung aktiviert. Dadurch wird Ihre IP-Adresse von Google innerhalb von Mitgliedsstaaten der Europäischen Union oder in anderen Vertragsstaaten des Abkommens über den Europäischen Wirtschaftsraum vor der Übermittlung in die USA gekürzt. Nur in Ausnahmefällen wird die volle IP-Adresse an einen Server von Google in den USA übertragen und dort gekürzt. Im Auftrag des Betreibers dieser Webseite wird Google diese Informationen benutzen, um Ihre Nutzung der Webseite auszuwerten, um Reports über die Webseiten-Aktivitäten zusammenzustellen und um weitere mit der Webseitennutzung und der Internetnutzung verbundene Dienstleistungen gegenüber dem Webseitenbetreiber zu erbringen. Die im Rahmen von Google Analytics von Ihrem Browser übermittelte IP-Adresse wird nicht mit anderen Daten von Google zusammengeführt.</p>
					<h3>Google-Maps</h3>
					<p>Wir setzen auf unserer Seite die Komponente „“Google Maps““ ein. „“Google Maps““ ist ein Service der Firma Google Inc., 1600 Amphitheatre Parkway, Mountain View, CA 94043 USA, nachfolgend „Google“.</p>
					<p>Bei jedem einzelnen Aufruf dieser Komponente wird von Google ein Cookie gesetzt, um bei der Anzeige der Seite, auf der die Komponente „Google Maps“ integriert ist, Nutzereinstellungen und -daten zu verarbeiten. Dieses Cookie wird im Regelfall nicht durch das Schließen des Browsers gelöscht, sondern läuft nach einer bestimmten Zeit ab, soweit es nicht von Ihnen zuvor manuell gelöscht wird.</p>
					<p>Wenn Sie mit dieser Verarbeitung Ihrer Daten nicht einverstanden sind, so besteht die Möglichkeit, den Service von „Google Maps“ zu deaktivieren und auf diesem Weg die Übertragung von Daten an Google zu verhindern. Dazu müssen Sie die Java-Script-Funktion in Ihrem Browser deaktivieren. Wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall „Google Maps“ nicht oder nur eingeschränkt nutzen können.</p>
					<p>Die Nutzung von „“Google Maps““ und der über „“Google Maps““ erlangten Informationen erfolgt gemäß den Google-Nutzungsbedingungen </p>
					<p><a href="http://www.google.de/intl/de/policies/terms/regional.html">http://www.google.de/intl/de/policies/terms/regional.html</a></p>
					<p>sowie der zusätzlichen Geschäftsbedingungen für „Google Maps“ </p>
					<p><a href="https://www.google.com/intl/de_de/help/terms_maps.html">https://www.google.com/intl/de_de/help/terms_maps.html</a></p>
					<h3>Browser Plugin</h3>
					<p>Sie können die Speicherung der Cookies durch eine entsprechende Einstellung Ihrer Browser-Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website vollumfänglich werden nutzen können. Sie können darüber hinaus die Erfassung der durch das Cookie erzeugten und auf Ihre Nutzung der Website bezogenen Daten (inkl. Ihrer IP-Adresse) an Google sowie die Verarbeitung dieser Daten durch Google verhindern, indem sie das unter dem folgenden Link verfügbare Browser-Plugin herunterladen und installieren: <a href="http://tools.google.com/dlpage/gaoptout?hl=de">http://tools.google.com/dlpage/gaoptout?hl=de.</a></p>
					<h3>Widerspruch gegen Datenerfassung</h3>
					<p>Sie können die Erfassung Ihrer Daten durch Google Analytics verhindern, indem Sie auf folgenden Link klicken. Es wird ein Opt-Out-Cookie gesetzt, der die Erfassung Ihrer Daten bei zukünftigen Besuchen dieser Website verhindert: <a onclick="alert('Google Analytics wurde deaktiviert');" href="javascript:gaOptout()">Google Analytics deaktivieren</a></p>
					<p>Mehr Informationen zum Umgang mit Nutzerdaten bei Google Analytics finden Sie in der Datenschutzerklärung von Google: <a href="https://support.google.com/analytics/answer/6004245?hl=de">https://support.google.com/analytics/answer/6004245?hl=de</a></p>

					<h3>Auftragsdatenverarbeitung</h3>
					<p>Wir haben mit Google einen Vertrag zur Auftragsdatenverarbeitung abgeschlossen und setzen die strengen Vorgaben der deutschen Datenschutzbehörden bei der Nutzung von Google Analytics vollständig um.</p>

					<h2>VII. Plugins und Tools</h2>
					<h3>Google reCAPTCHA</h3>
					<p>Auf dieser Webseite verwenden wir die reCAPTCHA-Funktion von Google LLC, 1600 Amphitheatre Parkway, Mountain View, CA 94043, USA („Google“). Diese Funktion dient vor allem zur Unterscheidung, ob eine Eingabe durch eine natürliche Person erfolgt oder missbräuchlich durch maschinelle und automatisierte Verarbeitung erfolgt. Der Dienst inkludiert den Versand der IP-Adresse und ggf. weiterer von Google für den Dienst von reCAPTCHA benötigter Daten an Google und erfolgt gemäß dem Art. 6 Abs. 1 lit. f DSGVO auf Basis unseres berechtigten Interesses an der Feststellung der individuellen Willensgetragenheit von Handlungen im Internet und der Vermeidung von Missbrauch und Spam.</p>
					<p>Google LLC mit Sitz in den USA ist für das US-Europäische Datenschutzübereinkommen „Privacy Shield“ zertifiziert, welches die Einhaltung des in der EU geltenden Datenschutzniveaus gewährleistet.</p>
					<p>Weiterführende Informationen zu Google reCAPTCHA sowie die Datenschutzerklärung von Google könnten Sie einsehen unter: <a href="https://www.google.com/intl/de/policies/privacy/">https://www.google.com/intl/de/policies/privacy/</a></p>

					<h2>VIII. SSL-Verschlüsselung</h2>
					<p>Diese Seite nutzt aus Gründen der Sicherheit und zum Schutz der Übertragung vertraulicher Inhalte, wie z.B. der Anfragen, die Sie an uns als Seitenbetreiber senden, eine SSL-Verschlüsselung. Eine verschlüsselte Verbindung erkennen Sie daran, dass die Adresszeile des Browsers von „http://“ auf „https://“ wechselt und an dem Schloss-Symbol in Ihrer Browserzeile. Wenn die SSL-Verschlüsselung aktiviert ist, können die Daten, die Sie dem Webseitenbetreiber übermitteln, nicht von Dritten mitgelesen werden.</p>

					<h2>IX. Rechte des Betroffenen</h2>
					<p>Werden personenbezogene Daten von Ihnen verarbeitet, sind Sie Betroffener i.S.d. DSGVO und es stehen Ihnen folgende Rechte gegenüber dem Verantwortlichen zu:</p>

					<h3>1. Auskunftsrecht gemäß Art. 15 DSGVO</h3>
					<p>Sie haben insbesondere ein Recht auf Auskunft über Ihre von uns verarbeiteten personenbezogenen Daten, die Verarbeitungszwecke, die Kategorien der verarbeiteten personenbezogenen Daten, die Empfänger oder Kategorien von Empfänger, gegenüber denen Ihre Daten offengelegt wurden oder werden, die geplante Speicherdauer bzw. die Kriterien für die Festlegung der Speicherdauer, das Bestehen eines Rechts auf Berichtigung, Löschung, Einschränkung der Verarbeitung, Widerspruch gegen die Verarbeitung, Beschwerde bei einer Aufsichtsbehörde, die Herkunft Ihrer Daten, wenn diese nicht durch uns bei Ihnen erhoben wurden, das Bestehen einer automatisierten Entscheidungsfindung einschließlich Profiling und ggf. aussagekräftige Informationen über die involvierte Logik und die Sie betreffende Tragweite und die angestrebten Auswirkungen einer solchen Verarbeitung. Ihnen steht das Recht zu, Auskunft darüber zu verlangen, ob die Sie betreffenden personenbezogenen Daten in ein Drittland oder an eine internationale Organisation übermittelt werden. In diesem Zusammenhang können Sie verlangen, über die geeigneten Garantien gemäß Art. 46 DSGVO im Zusammenhang mit der Übermittlung unterrichtet zu werden.</p>

					<h3>2. Recht auf Berichtigung gemäß Art 16 DSGVO</h3>
					<p>Sie haben ein Recht auf unverzügliche Berichtigung Sie betreffender unrichtiger Daten und/oder Vervollständigung Ihrer bei uns gespeicherten unvollständigen Daten.</p>

					<h3>3. Recht auf Einschränkung der Verarbeitung gemäß Art. 18 DSGVO</h3>
					<p>Unter den folgenden Voraussetzungen können Sie die Einschränkung der Verarbeitung Ihrer personenbezogenen Daten verlangen:</p>
					<ul>
						<li>wenn Sie die Richtigkeit der Sie betreffenden personenbezogenen Daten für eine Dauer bestreiten, die es dem Verantwortlichen ermöglicht, die Richtigkeit der personenbezogenen Daten zu überprüfen;</li>
						<li>die Verarbeitung unrechtmäßig ist und Sie die Löschung der personenbezogenen Daten ablehnen und stattdessen die Einschränkung der Nutzung der personenbezogenen Daten verlangen;</li>
						<li>der Verantwortliche die personenbezogenen Daten für die Zwecke der Verarbeitung nicht länger benötigt, Sie diese jedoch zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen benötigen, oder</li>
						<li>wenn Sie Widerspruch gegen die Verarbeitung gemäß Art. 21 Abs. 1 DSGVO eingelegt haben und noch nicht feststeht, ob die berechtigten Gründe des Verantwortlichen gegenüber Ihren Gründen überwiegen.</li>
					</ul>

					<h3>4. Recht auf Löschung gemäß Art. 17 DSGVO</h3>
					<p>Sie haben das Recht, die Löschung Ihrer personenbezogenen Daten bei Vorliegen der Voraussetzungen des Art. 17 Abs. 1 DSGVO zu verlangen. Dieses Recht besteht jedoch insbesondere dann nicht, wenn die Verarbeitung zur Ausübung des Rechts auf freie Meinungsäußerung und Information, zur Erfüllung einer rechtlichen Verpflichtung, aus Gründen des öffentlichen Interesses oder zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen erforderlich ist.</p>

					<h3>5. Recht auf Unterrichtung gemäß Art. 19 DSGVO</h3>
					<p>Haben Sie das Recht auf Berichtigung, Löschung oder Einschränkung der Verarbeitung gegenüber dem Verantwortlichen geltend gemacht, ist dieser verpflichtet, allen Empfängern, denen die Sie betreffenden personenbezogenen Daten offengelegt wurden, diese Berichtigung oder Löschung der Daten oder Einschränkung der Verarbeitung mitzuteilen, es sei denn, dies erweist sich als unmöglich oder ist mit einem unverhältnismäßigem Aufwand verbunden. Ihnen steht gegenüber dem Verantwortlichen das Recht zu, über diese Empfänger unterrichtet zu werden.</p>

					<h3>6. Recht auf Datenübertragbarkeit gemäß Art. 20 DSGVO</h3>
					<p>Sie haben das Recht, Ihre personenbezogenen Daten, die Sie uns bereitgestellt haben, in einem strukturierten, gängigen und maschinenlesbaren Format zu erhalten oder die Übermittlung an einen anderen Verantwortlichen zu verlangen, soweit dies technisch machbar ist.</p>

					<h3>7. Widerspruchsrecht</h3>
					<p>Sie haben das Recht, aus Gründen, die sich aus ihrer besonderen Situation ergeben, jederzeit gegen die Verarbeitung der Sie betreffenden personenbezogenen Daten, die aufgrund von Art 6 Ab. 1 lit. e oder f DSGVO erfolgt, Widerspruch einzulegen.
					Der Verantwortliche verarbeitet die Sie betreffenden personenbezogen Daten nicht mehr, es sei denn, er kann zwingende schutzwürdige Gründe für die Verarbeitung nachweisen, die Ihre Interessen, Rechte und Freiheiten überwiegen, oder die Verarbeitung dient der Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen.</p>

					<h3>8. Recht auf Widerruf gemäß Art. 7 Abs. 3 DSGVO</h3>
					<p>Sie haben das Recht, Ihre datenschutzrechtliche Einwilligungserklärung jederzeit zu widerrufen. Durch den Widerruf der Einwilligung wird die Rechtmäßigkeit der aufgrund der Einwilligung bis zum Widerruf erfolgten Verarbeitung nicht berührt.</p>

					<h3>9. Recht auf Beschwerde gemäß Art. 77 DSGVO</h3>
					<p>Wenn Sie der Ansicht sind, dass die Verarbeitung der Sie betreffenden personenbezogenen Daten gegen die DSGVO verstößt, haben Sie – unbeschadet eines anderweitigen verwaltungsrechtlichen oder gerichtlichen Rechtsbehelfs – das Recht auf Beschwerde bei einer Aufsichtsbehörde, insbesondere in dem Mitgliedsstaat Ihres Aufenthaltsortes, Ihres Arbeitsplatzes oder des Ortes des mutmaßlichen Verstoßes.</p>

					<h2>X. Widerspruch Werbe-Mails</h2>
					<p>Der Nutzung von im Rahmen der Impressumspflicht veröffentlichen Kontaktdaten zur Übersendung von nicht ausdrücklich angeforderter Werbung  und Informationsmaterialien wird hiermit widersprochen. Die Betreiber der Seiten behalten sich ausdrücklich rechtliche Schritte im Falle der unverlangten Zusendung von Werbeinformationen, etwa durch Spam-E-Mails, vor.</p>


					<p><a href="index.php" class="button">Zurück</a></p>
				</div>
			</div>
		<footer class="dark">
			<nav>
				<ul>
					<li><a href="impressum.php">Impressum</a></li>
					<li><a href="datenschutz.php">Datenschutz</a></li>
				</ul>
			</nav>
		</footer>
	</body>
</html>