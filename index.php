<!DOCTYPE html>
<html>
<head>
	<title>Solarplexus-Warteseite</title>
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
</head>
	<body>
		<div class="columns">
			<div class="content">
				<header><img src="img/logo.svg" alt="" class="logo"></header>
				<div class="main">
					<h1>Willkommen bei SOLARplexus</h1>
					<p>Unsere Website befindet sich derzeit noch im Aufbau. In Kürze finden Sie hier alle wichtigen Informationen zu unseren innovativen In-Dach-Solarziegeln und weitere Informationen zu Planungs- und Serviceleistungen rund um Ihr PV-Glasdach.</p>
					<p>Nehmen Sie mit uns Kontakt auf!<br />
					Senden Sie uns einfach eine E-Mail mit Ihrem Anliegen und wir 
					melden uns umgehend bei Ihnen.</p>
					<p>Für Ihre Fragen und  Anliegen stehen  wir Ihnen natürlich 
					auch persönlich jederzeit zur Verfügung.</p>
					<a class="button" href="mailto:reiner.mack@solarplexus.group">E-Mail senden</a>
					<div class="columns">
						<div class="contact">
							<p>Reiner Mack:</p>
							<p>Mobil: <a href="phone:01716237375">0171 6237375</a></p>
							<a href="mailto:reiner.mack@solarplexus.group">reiner.mack@solarplexus.group</a>
						</div>
						<div class="contact">
							<p>Bernd Gieseler:</p>
							<p>Mobil: <a href="phone:015123564080">0151 23564080</a></p>
							<a href="mailto:bernd.gieseler@solarplexus.group">bernd.gieseler@solarplexus.group</a>
						</div>
					</div>
				</div>
			</div>
			<div class="filler">
				<img src="img/background.png" alt="">
			</div>
		</div>
		<footer>
			<nav>
				<ul>
					<li><a href="impressum.php">Impressum</a></li>
					<li><a href="datenschutz.php">Datenschutz</a></li>
				</ul>
			</nav>
		</footer>
	</body>
</html>