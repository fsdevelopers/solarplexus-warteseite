<!DOCTYPE html>
<html>
<head>
	<title>Solarplexus-Warteseite</title>
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
</head>
	<body>

		<div class="columns">
			<div class="content">
				<header><img src="img/logo.svg" alt="" class="logo"></header>
				<div class="main">
					<h1>SOLARplexus Dach GmbH</h1>
					<p>
						Am Witschenberg 2 57271 Hichenbach
						T:+49 151 23564080
					</p>
					<p>
						<a href="mailto:info@solarplexus.group">info@solarplexus.group</a>
					 	<a href="mailto:www.solarplexus.group">www.solarplexus.group</a> 
					</p>
					<p>
						Geschäftsführer: 
						Bernd Gieseler, Reiner Mack
					</p>
					<p>
						Registergericht: 
						Amtsgericht Siegen,
					</p>
					<p>
						Registernummer: 
						HRB 12230
					</p>
					<p>
						Umsatzsteuer-Identifikationsnummer gemäß §27a Umsatzsteuergesetz: USt-Id.-Nr.: 
						DE 329 778 360 
					</p>

					<a href="index.php" class="button">Zurück</a>
				</div>
			</div>
			<div class="filler">
				<img src="img/background.png" alt="">
			</div>
		</div>
		<footer>
			<nav>
				<ul>
					<li><a href="impressum.php">Impressum</a></li>
					<li><a href="datenschutz.php">Datenschutz</a></li>
				</ul>
			</nav>
		</footer>
	</body>
</html>